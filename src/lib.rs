use futures::{StreamExt, TryStreamExt};
use select::document::Document;
use select::predicate::Class;
use serde::Serialize;
use std::error::Error;
use std::iter;
use thirtyfour::error::WebDriverError;
use thirtyfour::prelude::*;
use tokio;

pub mod config;
use config::Config;

pub fn scrape(url: &str, booking_number: &str) -> Result<String, Box<dyn Error>> {
    let scrape_url = format!("{}?booking={}", url, booking_number);
    let response = reqwest::blocking::get(scrape_url)?;
    assert!(response.status().is_success());

    Document::from_read(dbg!(response))
        .unwrap()
        .find(Class("data"))
        .for_each(|x| println!("{:?}", x));

    Ok(String::new())
}

pub fn scrape_source01(
    config: &Config,
    booking_number: &str,
) -> Result<Vec<Container>, Box<dyn Error>> {
    let scrape_url = format!("{}?booking={}", config.source01.url, booking_number);
    let containers = request_source01(config, scrape_url.as_str())?;
    Ok(containers)
}

#[derive(Debug, Serialize)]
pub struct Container {
    size_type: String,
    container_number: String,
    current_status: Status,
    events: Vec<Event>,
}

#[derive(Debug, Serialize)]
pub struct Status {
    status_type: String,
    date: String,
    place: String,
}

#[derive(Debug, Serialize)]
pub struct Event {
    event_type: String,
    place: String,
    date: String,
    time: String,
    transport: String,
    voyage: String,
}

#[tokio::main]
async fn request_source01(config: &Config, url: &str) -> Result<Vec<Container>, Box<dyn Error>> {
    let mut caps = DesiredCapabilities::chrome();
    caps.add_chrome_arg("--enable-automation")?;
    caps.add_chrome_arg("--headless")?;
    let driver = WebDriver::new("http://localhost:4444/wd/hub", &caps).await?;

    driver.delete_all_cookies().await?;
    driver.get(dbg!(url)).await?;

    let privacy_preference_button = driver
        .find_element(By::Css(
            config.source01.privacy_preference_button_selector.as_str(),
        ))
        .await?;

    privacy_preference_button.click().await?;

    let data_table = driver
        .find_elements(By::Css(config.source01.data_table_row_selector.as_str()))
        .await?;
    let containers: Vec<Container> = futures::stream::iter(data_table)
        .then(|row| async move {
            let fields = row.find_elements(By::Css("td > span")).await?;

            let size_type = fields[0].text().await?;
            let container_number = fields[1]
                .text()
                .await?
                .chars()
                .filter(|&c| c != ' ')
                .collect();
            let status_type = fields[2].text().await?;
            let date = fields[3].text().await?;
            let place = fields[4].text().await?;

            Ok::<Container, WebDriverError>(Container {
                size_type,
                container_number,
                current_status: Status {
                    status_type,
                    date,
                    place,
                },
                events: vec![],
            })
        })
        .try_collect()
        .await?;

    let containers = futures::stream::iter(
        containers
            .into_iter()
            .enumerate()
            .zip(iter::repeat(&driver)),
    )
    .then(|((index, container), localdriver)| async move {
        localdriver.execute_script("window.scrollTo(0, 0);").await?;

        let data_table_rows = localdriver
            .find_elements(By::Css(config.source01.data_table_row_selector.as_str()))
            .await?;
        let input_selection = data_table_rows[index]
            .find_element(By::Css(config.source01.input_selection_selector.as_str()))
            .await?;
        input_selection.click().await?;

        let details_button = localdriver
            .find_element(By::Css(
                config.source01.container_details_button_selector.as_str(),
            ))
            .await?;
        details_button.click().await?;

        let data_table = localdriver
            .find_elements(By::Css(config.source01.data_table_row_selector.as_str()))
            .await?;
        let events = futures::stream::iter(data_table)
            .then(|row| async move {
                let fields = row.find_elements(By::Css("td > span")).await?;

                let event_type = fields[0].text().await?;
                let place = fields[1].text().await?;
                let date = fields[2].text().await?;
                let time = fields[3].text().await?;
                let transport = fields[4].text().await?;
                let voyage = fields[5].text().await?;

                Ok::<Event, WebDriverError>(Event {
                    event_type,
                    place,
                    date,
                    time,
                    transport,
                    voyage,
                })
            })
            .try_collect()
            .await?;

        localdriver.back().await?;

        Ok::<Container, WebDriverError>(Container {
            events,
            ..container
        })
    })
    .try_collect()
    .await?;

    Ok(containers)
}
