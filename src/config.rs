use serde::Deserialize;
use serde_yaml;
use std::error::Error;
use std::fs;

#[derive(Debug, Deserialize)]
pub struct Config {
    pub source01: Source01,
}

#[derive(Debug, Deserialize)]
pub struct Source01 {
    pub url: String,
    pub privacy_preference_button_selector: String,
    pub data_table_row_selector: String,
    pub container_details_button_selector: String,
    pub input_selection_selector: String,
}

pub fn read_config(path: &str) -> Result<Config, Box<dyn Error>> {
    let input = fs::read_to_string(path)?;
    let config: Config = serde_yaml::from_str(input.as_str())?;
    Ok(config)
}
