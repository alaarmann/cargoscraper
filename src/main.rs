use std::env;
use std::error::Error;
use std::process;

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() != 2 {
        eprintln!("Usage: {} <booking_number>", args[0]);

        process::exit(1);
    }

    let booking_number = &args[1];

    let result = cargoscraper::config::read_config("config.yaml").and_then(|config| {
        cargoscraper::scrape_source01(&config, booking_number).and_then(|retrieved_data| {
            serde_json::to_string(&retrieved_data).map_err(|error| {
                let err: Box<dyn Error + Send + Sync> = From::from(error);
                err as Box<dyn Error>
            })
        })
    });

    if let Err(e) = result {
        eprintln!("Parse error: {}", e);

        process::exit(1);
    }

    println!("{}", result.unwrap());
}
